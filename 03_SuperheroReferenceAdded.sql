USE SuperheroDB;

ALTER TABLE Assistant
ADD SuperheroId INT;

ALTER TABLE Assistant
ADD CONSTRAINT FK_Superhero_Assistant

FOREIGN KEY(SuperheroId) REFERENCES Superhero(SuperheroId);
