USE SuperheroDB;

CREATE TABLE SuperheroPowers(
SuperheroId INT FOREIGN KEY REFERENCES Superhero(SuperheroId),
PowerId INT FOREIGN KEY REFERENCES Powers(PowerId),
PRIMARY KEY(SuperheroId, PowerId)
);