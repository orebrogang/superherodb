USE SuperheroDB;

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Bruce Wayne', 'Batman', 'Gotham City'),
	   ('Clark Kent', 'Superman', 'Krypton'),
	   ('Peter Parker', 'Spiderman', 'Queens');