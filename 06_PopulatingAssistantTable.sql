USE SuperheroDB;

INSERT INTO Assistant(Name, SuperheroId)
VALUES ('Mary Jane', 3),
	   ('Alfred', 1),
	   ('Lex Luther', 2);