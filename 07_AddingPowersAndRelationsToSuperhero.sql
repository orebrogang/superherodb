USE SuperheroDB;

INSERT INTO Powers(Name, Description)
VALUES ('Laser Beam Eyes', 'Able to shoot laser beams from eyes'),
	   ('Supernatural Strength', 'Gains supernatural strength unlike any human being'),
	   ('Spiderweb', 'Can shoot spiderweb from hands'),
	   ('Wealth', 'Has enough money to build amazing combat suits');

INSERT INTO SuperheroPowers(SuperheroId, PowerId)
VALUES (1,4),
	   (1,2),
	   (2,1),
	   (2,2),
	   (3,3),
	   (3,2);
