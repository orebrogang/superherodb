# Superhero Database

##### Developed by Alexander Majoros, Carl Jägerhill and Anders Hansen-Haug.

# Pre-requisites
##### 1. Install Sql-Server Management Studio

# Queries
##### 1. Create database
##### 2. Create tables: Superhero, Assistant, Power
##### 3. Adding FORIGN KEY relationships between Superhero and Assistant
##### 4. Adding FORIGN KEY relationships between Superhero and Power
##### 5. Populating Superhero table with data
##### 6. Populating Assitant table with data
##### 7. Creating table for linking Superhero with Powers
##### 8. Updating a Superhero
##### 9. Deleting a Assistant
